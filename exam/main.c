#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int exp_diff_equation // set up differential equation, which can be solved by gsl
(double t, const double y[], double dydt[], void * params)
{
	dydt[0]=y[0];
	return GSL_SUCCESS;
}

double exp_function(double x){
		

	gsl_odeiv2_system sys; // define the ODE (ordinary diff equation) system
	sys.function = exp_diff_equation; // function to solve
	sys.jacobian = NULL; // we dont provide a jacobian
	sys.dimension = 1; // set the dimension of the problem
	sys.params = NULL; // we done provide any parameters

	double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x); // define accuracy, the error accepted, and initial step size
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new // allocate memory for the driver
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double t=0,y[1]={1}; // define startpoint and vector for variables
	gsl_odeiv2_driver_apply(driver,&t,x,y); // driver returns solution to diff eq.

	gsl_odeiv2_driver_free(driver); // free memory allocated for driver
	return y[0]; 
}

double expReducedFunction(double x){ // reduces argument to 0 < x <= 1
	if(x<0){
		return 1/expReducedFunction(-x);
	}
	if(x>=1){
		return pow(expReducedFunction(x/2),2);
	}
	return exp_function(x);
}

int main(){
	double a = -3; 
	double b = 3; 
	double dx = 0.1;
	for(double x=a; x<b+dx; x+=dx)printf("%g %g %g\n",x,expReducedFunction(x),exp(x));
	return 0;
}

	
