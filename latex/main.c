#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>


int err_diff_equation // set up differential equation, which can be solved by gsl
(double t, const double y[], double dydt[], void * params)
{
	dydt[0]=2/sqrt(3.1415)*exp(-pow(t,2.0));
	return GSL_SUCCESS;
}

double err_function(double x){
	gsl_odeiv2_system sys; // define the ODE (ordinary diff equation) system
	sys.function = err_diff_equation; // function to solve
	sys.jacobian = NULL; // we dont provide a jacobian
	sys.dimension = 1; // set the dimension of the problem
	sys.params = NULL; // we done provide any parameters

	double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x); // define accuracy, the error accepted, and initial step size
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new // alloceate memory for the driver
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double t=0,y[1]={0}; // define startpoint and vector for variables
	gsl_odeiv2_driver_apply(driver,&t,x,y); // driver returns solution to diff eq.

	gsl_odeiv2_driver_free(driver); // free memory allocated for driver
	return y[0]; // returns err(x)
}

int main(int argc, char* argv[]){
	double a = atof(argv[1]);
	double b = atof(argv[2]);
	double dx = atof(argv[3]);
	for(double x=a; x<b+dx; x+=dx)printf("%g %g\n",x,err_function(x));
	return 0;
}
	
