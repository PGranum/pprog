#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "nvector.h"
#include <assert.h>

nvector* nvector_alloc(int n){
	nvector* v = malloc(sizeof(nvector));
 	(*v).size = n;
  	(*v).data = malloc(n*sizeof(double));
  	if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  	return v;
}

void nvector_free(nvector* v){
	free(v);
}

void nvector_set(nvector* v, int i, double value){
	assert(0 <= i && i < (*v).size);
	(*v).data[i] = value;
}

double nvector_get(nvector* v, int i){
	return (*v).data[i];
}

double nvector_dot_product(nvector* u, nvector* v){
	int n = (*u).size;
	double dot_prod = 0;
	double u_i;
	double v_i;
	for (int i = 0; i < n; ++i)
	{
		u_i = (*u).data[i];
		v_i = (*v).data[i];
		dot_prod += u_i*v_i;		
	}
	return dot_prod;
}

