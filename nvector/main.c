#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "nvector.h"
#define RND (double)rand()/RAND_MAX

int main(){

	int n = 5;

	printf("main: testing nvector_alloc ...\n");
	nvector* v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	printf("main: testing nvector_set ...\n");
	int noOfElement = 3;
	double value = 3.14;
	nvector_set(v,noOfElement,value);
	printf("Element no. %i of the vector v is %g, and i should be %g\n",noOfElement+1,(*v).data[noOfElement],value);

	printf("main: testing nvector_get ...\n");
	double getElement = nvector_get(v,noOfElement);
	printf("Element no. %i of the vector v is %g, and i should be %g\n",noOfElement+1,getElement,value);
	
	printf("main: testing nvector_dot_product ...\n");
	nvector* u = nvector_alloc(n);
	nvector_set(u,1,2);
	nvector_set(v,1,2);
	nvector_set(u,3,2);
	double dotP = nvector_dot_product(v,u);
	printf("the dot product of v and u is %g\n",dotP);

	double testDotP;
	for(int i = 0; i < n; i++){
		printf("n_%i = %g, u_%i = %g\n",i, nvector_get(v,i), i, nvector_get(u,i));
		testDotP += nvector_get(v,i)*nvector_get(u,i);
	}
	printf("and it should be %g\n", testDotP);

	nvector_free(v);
	
	return 0;
}

