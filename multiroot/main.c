#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_multiroots.h>
#include<assert.h>
#define ALG gsl_multiroot_fsolver_hybrids

double Fe(double e, double r);

int master(const gsl_vector * x, void * params, gsl_vector * f){
	double e = gsl_vector_get(x,0);
	double rmax = *(double*)params;
	gsl_vector_set(f,0,Fe(e,rmax));
	return GSL_SUCCESS;
}

int RosenbrockGrad (const gsl_vector *xRos, void *params, gsl_vector * f){
	double x = gsl_vector_get(xRos,0);
	double y = gsl_vector_get(xRos,1);
	double dx = -1*2*(1-x)+100*(-2*x)*2*(y-pow(x,2.0)); // (1-x)² + 100*(y-x²)²
	double dy = 100*2*(y-pow(x,2.0));
	gsl_vector_set(f,0,dx);
	gsl_vector_set(f,1,dy);	
return GSL_SUCCESS;
}

int main(){

const int dimRos = 2;
gsl_multiroot_function Ros;
Ros.f = RosenbrockGrad;
Ros.n = dimRos;
Ros.params = NULL;

gsl_multiroot_fsolver *sRos = gsl_multiroot_fsolver_alloc (ALG, dimRos);
gsl_vector * xRos = gsl_vector_alloc(dimRos);
gsl_vector_set(xRos,0,0.8);
gsl_vector_set(xRos,1,0.8);
gsl_multiroot_fsolver_set (sRos, &Ros, xRos);

int iterRos=0,statusRos;
const double accRos=1e-3;
do{
	iterRos++;
	int flagRos=gsl_multiroot_fsolver_iterate(sRos);
	if(flagRos!=0)break;
	statusRos = gsl_multiroot_test_residual (sRos->f, accRos);
	fprintf(stderr
		,"iter= %2i, x= %8g, y= %8g, grad_x= %g, grad_y=%g\n"
		,iterRos
		,gsl_vector_get(sRos->x,0)
		,gsl_vector_get(sRos->x,1)
		,gsl_vector_get(sRos->f,0)
		,gsl_vector_get(sRos->f,1)		
		);
}while(statusRos == GSL_CONTINUE && iterRos<99);

fprintf(stderr,"\n");
	
gsl_vector_free(xRos);
gsl_multiroot_fsolver_free (sRos);

// -----------------------------------------------------------------------

const int dim = 1; // dimension of problem is 1
const double rmax = 8; // number much bigger than bohr rad
gsl_multiroot_function M; // define master function to minimize
M.f = master;
M.n = dim;
M.params = (void*)&rmax;

gsl_multiroot_fsolver *s = gsl_multiroot_fsolver_alloc (ALG, dim);
gsl_vector * x = gsl_vector_alloc(dim);
gsl_vector_set(x,0,-.8);
gsl_multiroot_fsolver_set (s, &M, x);

int iter=0,status;
const double acc=1e-3;
do{
	iter++;
	int flag = gsl_multiroot_fsolver_iterate(s);
	if(flag != 0)break;
	status = gsl_multiroot_test_residual (s->f, acc);
	fprintf(stderr
		,"iter= %2i, E= %8f, Fe(%g) = %g\n"
		,iter
		,gsl_vector_get(s->x,0)
		,rmax
		,gsl_vector_get(s->f,0)
		);
}while(status == GSL_CONTINUE && iter<99);

double e = gsl_vector_get(s->x,0);
for(double r = 0; r <= rmax; r += rmax/100){
	printf("%g %g %g\n",r,Fe(e,r),r*exp(-r));
}
	
gsl_vector_free(x);
gsl_multiroot_fsolver_free (s);
return 0;
}
