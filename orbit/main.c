#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int diff_equation (double t, const double y[], double dydt[], void * params)
{
	dydt[0] = y[0]*(1-y[0]);
	return GSL_SUCCESS;
}

int orbit_diff_eq (double phi, const double y[], double dydt[], void * params)
{
	double e = *(double *) params;
	dydt[0] = y[1];
	dydt[1] = 1 + e*y[0]*y[0] - y[0];
	return GSL_SUCCESS;
}

double function(double x)
{
	gsl_odeiv2_system sys; //type sys
	sys.function = diff_equation;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;

	double acc = 1e-6, eps = 1e-6, hstart=copysign(0.1,x);

	gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new
		(&sys, gsl_odeiv2_step_rkf45, hstart, acc, eps);

	double t = 0, y[1] = {0.5};
	gsl_odeiv2_driver_apply(driver, &t, x, y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

double orbit_function(double phi, double e, double dustart)
{
	gsl_odeiv2_system sys; //type sys
	sys.function = orbit_diff_eq;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void *) &e;

	double acc = 1e-6, eps = 1e-6, hstart=copysign(0.1,phi);

	gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new
		(&sys, gsl_odeiv2_step_rkf45, hstart, acc, eps);

	double t = 0, y[2] = {1,dustart};
	gsl_odeiv2_driver_apply(driver, &t, phi, y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int main()
{
	double a = 0, b = 3, dx = 0.05;
	for(double x = a; x<=b; x+=dx)(printf("%g %g %g\n",x,function(x),1.0/(1.0+exp(-x))));
	printf("\n\n");

	b = 100;
	double e = 0.0;
	double dustart = 0.0;
	for(double x = a; x<=b; x+=dx)(printf("%g %g\n",x,orbit_function(x,e,dustart)));

	printf("\n\n");
	e = 0.0;
	dustart = -0.5;
	for(double x = a; x<=b; x+=dx)(printf("%g %g\n",x,orbit_function(x,e,dustart)));

	printf("\n\n");
	e = 0.01;
	dustart = -0.5;
	for(double x = a; x<=b; x+=dx)(printf("%g %g\n",x,orbit_function(x,e,dustart)));


	return 0;
}
