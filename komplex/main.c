#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"komplex.h"

int main(){
komplex z;
double x = 1;
double y = 2;

printf("test of komplex_set\n");
komplex_set(&z, x, y);
printf("z = %g +i(%g)\n",z.re,z.im);

printf("test of komplex_new\n");
z = komplex_new(x,y);
printf("z = %g +i(%g)\n",z.re,z.im);

printf("test of komplex_print\n");
komplex_print("pring z:",z);

printf("test of komplex_add\n");
komplex a = {1,2};
komplex b = {3,4};
komplex sum = komplex_add(a,b);
printf("(%g+i%g)+(%g+i%g) = %g +i(%g)\n",a.re,a.im,b.re,b.im,sum.re,sum.im);

printf("test of komplex_sub\n");
komplex diff = komplex_sub(a,b);
printf("(%g+i%g)-(%g+i%g) = %g +i(%g)\n",a.re,a.im,b.re,b.im,diff.re,diff.im);

printf("test of komplex_conjugate\n");
komplex z_conj = komplex_conjugate(z);
printf("z*=%g+i(%g)\n",z_conj.re,z_conj.im);

return 0;
}
