#include <stdio.h>
#include <math.h>
#include <complex.h>

int main(){
	double n = 5;
	double gn = tgamma(n);
	printf("Gamma function Gamma(%g) = %g\n",n,gn);

	n = 0.5;
	double jn = j1(n);
	printf("First bessel function J_1(%g) = %g\n",n,jn);

	n = -2;
	double complex y = csqrt(n);
	printf("The sqrt of %g = %g+i(%g)\n",n,creal(y),cimag(y));

	printf("exp(i) = %g+i(%g)\n",creal(cexp(I)),cimag(cexp(I)));
	printf("exp(i*pi) = %g\n",creal(cexp(I*M_PI)));
	printf("i^e = %g+i(%g)\n",creal(cpow(I,M_E)),cimag(cpow(I,M_E)));

	float a = 0.1111111111111111111111111111;
	double b = 0.1111111111111111111111111111;
	long double c = 0.1111111111111111111111111111L;
	printf("Print 0.1111111111111111111111111111 in different types \n");
	printf("Float: %.25g \nDouble: %.25lg \nLong double: %.25Lg\n",a,b,c);

	int bits=sizeof(void*)*8;
	printf("This system is %i bits\n",bits);

	return 0;
}
