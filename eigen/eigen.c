#include<stdio.h>
#include<gsl/gsl_sf.h>
#include<gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>

const double pi = 3.14159265358979323844;

int main(){

double data1[] = 	{6.13, -2.90,  5.86,
			 8.08, -6.31, -3.89,
			-4.36,  1.00,  0.19};
double data3[] = 	{6.13, -2.90,  5.86,
			 8.08, -6.31, -3.89,
			-4.36,  1.00,  0.19};

double data2[] = { 6.23, 5.37, 2.29 };

gsl_matrix_view m = gsl_matrix_view_array (data1, 3, 3);
gsl_matrix_view m2 = gsl_matrix_view_array (data3, 3, 3);

gsl_vector_view b = gsl_vector_view_array (data2, 3);

gsl_vector *vec = gsl_vector_alloc (3);
int alpha;

gsl_permutation * p = gsl_permutation_alloc (3);

gsl_linalg_LU_decomp (&m.matrix, p, &alpha);

gsl_linalg_LU_solve (&m.matrix, p, &b.vector, vec);
printf ("x = \n");
fprintf(stderr,"the solution to the 3x3 matrix system is:\n");
gsl_vector_fprintf (stderr, vec, "%g");

fprintf(stderr,"\nCheck that solution reproduces right hand side vector\n");
gsl_vector *check = gsl_vector_alloc (3);
gsl_blas_dgemv(CblasNoTrans, 1.0, &m2.matrix, vec, 0.0, check);
gsl_vector_fprintf (stderr, check, "%g");


gsl_permutation_free (p);
gsl_vector_free (vec);
gsl_vector_free (check);

int n=20;
double s=1.0/(n+1);
gsl_matrix *H = gsl_matrix_calloc(n,n); //allocate space for matrix
for(int i=0;i<n-1;i++){ // fill in matrix elements
  gsl_matrix_set(H,i,i,-2);
  gsl_matrix_set(H,i,i+1,1);
  gsl_matrix_set(H,i+1,i,1);
  }
gsl_matrix_set(H,n-1,n-1,-2); //fill in last element in diagonal
gsl_matrix_scale(H,-1/s/s); //scale the matrix	

gsl_eigen_symmv_workspace* w =  gsl_eigen_symmv_alloc (n); //allocates memory for workspace
gsl_vector* eval = gsl_vector_alloc(n); // allocates memory for vector for eigenvalues
gsl_matrix* evec = gsl_matrix_calloc(n,n); // allocates memory for matrix
gsl_eigen_symmv(H,eval,evec,w); //finds eigenvalues and eigenfunctions

gsl_eigen_symmv_sort(eval,evec,GSL_EIGEN_SORT_VAL_ASC); //sorts the eigenvalues in order

fprintf (stderr, "i   exact   calculated\n"); 
for (int k=0; k < n/3; k++){
    double exact = pi*pi*(k+1)*(k+1); // calculate the exact values
    double calculated = gsl_vector_get(eval,k); // get the calculated eigval for the k'th vector
    fprintf (stderr, "%i   %g   %g\n", k, exact, calculated); // writes results to log
}

// prepare to plot the 3 lowest eigenfunctions
for(int k=0;k<3;k++){
  printf("%i %f\n",0,0.0); // wave function is zero on edge of box potential
  for(int i=0;i<n;i++) printf("%g %f\n",(i+1.0)/(n+1),gsl_matrix_get(evec,i,k)); // write data to data file
  printf("%i %f\n",1,0.0); // wave function is zero on edge of box potential
  printf("\n\n"); // make double blank to begin new data set
  }

// free up memory
	gsl_matrix_free(H);
	gsl_matrix_free(evec);
	gsl_vector_free(eval);
	gsl_eigen_symmv_free(w);

return 0;
}
