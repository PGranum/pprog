#include<gsl/gsl_multimin.h>
#include<assert.h>

struct experimental_data {int n; double *t,*y,*e;};

double Rosenbrock (const gsl_vector *var, void *params){
	double x = gsl_vector_get(var,0);
	double y = gsl_vector_get(var,1);
	return pow(1-x,2.0) + 100*pow(y-pow(x,2.0),2.0);
}

double deviation_between_model_and_experiment (const gsl_vector *x, void *params) {
	double A=gsl_vector_get(x,0);
	double T=gsl_vector_get(x,1);
	double B=gsl_vector_get(x,2);
	struct experimental_data *p = (struct experimental_data*) params;
	int     n=p->n;
	double *t=p->t;
	double *y=p->y;
	double *e=p->e;
	double sum=0;
	#define model(t) A*exp(-(t)/T)+B
	for(int i=0;i<n;i++) sum+=pow( (model(t[i]) - y[i]) /e[i] ,2);
	return sum;
}

int main(){
	fprintf(stderr,"# solve problem 1:\n");
	gsl_multimin_function Ros;
	Ros.f = Rosenbrock; // the Rosenbrock function
	Ros.n = 2; // 2 variables: x and y
	Ros.params = NULL; // no parameters provided

	gsl_vector *startRos = gsl_vector_alloc(Ros.n); // allocate memory for vector, which is to contain start guess
	gsl_vector_set(startRos,0,0.8); // start guess for x
	gsl_vector_set(startRos,1,0.8); // start guess for y

	gsl_vector *stepRos = gsl_vector_alloc(Ros.n); // allocate memory for vector, which is to contain step sizes
	gsl_vector_set(stepRos,0,0.1); // step size for variable x
	gsl_vector_set(stepRos,1,0.1); // step size for variable y


	gsl_multimin_fminimizer *stateRos =
		gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,Ros.n); // allocate memory for minfunc
	gsl_multimin_fminimizer_set (stateRos, &Ros, startRos, stepRos); // set input

	int iterRos=0, statusRos;
	do{
		iterRos++;
		int iteration_statusRos = gsl_multimin_fminimizer_iterate(stateRos);
		if(iteration_statusRos != 0){
			fprintf(stderr,"unable to improve\n");
			break;
			}

		double accRos=0.01; // set desired accuracy
		statusRos = gsl_multimin_test_size(stateRos->size, accRos); // check if within acc
		if( statusRos == GSL_SUCCESS ) fprintf(stderr,"converged\n"); // end if within accuracy
		fprintf(stderr,"iter= %3i  ",iterRos); // print result
		fprintf(stderr,"x= %.3f  ",gsl_vector_get(stateRos->x,0));
		fprintf(stderr,"y= %.3f  ",gsl_vector_get(stateRos->x,1));
		fprintf(stderr,"\n");

	}while( statusRos == GSL_CONTINUE && iterRos < 100); // max 100 iterations

	double xRos=gsl_vector_get(stateRos->x,0); // save first variable to xRos
	double yRos=gsl_vector_get(stateRos->x,1); // save second variable to yRos
	

	printf("# (1-x)² + 100*(y-x²)²\n");
	printf("# minimum at x = %g and y = %g",xRos,yRos);
	
	// free up memory
	gsl_multimin_fminimizer_free(stateRos);
	gsl_vector_free(startRos);
	gsl_vector_free(stepRos);

	//------------------------------------------------------------------

	// data points
	double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n=sizeof(t)/sizeof(t[0]); // number of data points

	assert( n==sizeof(y)/sizeof(y[0]) && n==sizeof(e)/sizeof(e[0])); // check if vectors have same length
	fprintf(stderr,"# solve problem 2:\n");
	fprintf(stderr,"number of experimental points: %i\n",n);
	
	printf("# t[i], y[i], e[i]\n");
	for(int i=0;i<n;i++) printf("%g %g %g\n",t[i],y[i],e[i]); // write data to data file
	printf("\n\n");

	struct experimental_data params; // fill in data points as parameters
	params.n=n;
	params.t=t;
	params.y=y;
	params.e=e;

	gsl_multimin_function F;
	F.f=deviation_between_model_and_experiment; // the function to minimize
	F.n=3; // 3 variables A, T, B
	F.params=(void*)&params; // give params as parameters

	gsl_vector *start = gsl_vector_alloc(F.n); // start point for variables
	gsl_vector_set(start,0,3);
	gsl_vector_set(start,1,2);
	gsl_vector_set(start,2,0);

	gsl_vector *step = gsl_vector_alloc(F.n); // step sizes for variables
	gsl_vector_set(step,0,2);
	gsl_vector_set(step,1,2);
	gsl_vector_set(step,2,2);

	gsl_multimin_fminimizer *state =
		gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,F.n); // allocate memory for minfunc
	gsl_multimin_fminimizer_set (state, &F, start, step); // set input

	int iter=0, status;
	do{
		iter++;
		int iteration_status = gsl_multimin_fminimizer_iterate(state);
		if(iteration_status != 0){
			fprintf(stderr,"unable to improve\n");
			break;
			}

		double acc=0.01; // set desired accuracy
		status = gsl_multimin_test_size(state->size, acc);
		if( status == GSL_SUCCESS ) fprintf(stderr,"converged\n"); // end if within acc
		fprintf(stderr,"iter= %3i  ",iter); // prints results
		fprintf(stderr,"A= %.3f  ",gsl_vector_get(state->x,0));
		fprintf(stderr,"T= %.3f  ",gsl_vector_get(state->x,1));
		fprintf(stderr,"B= %.3f  ",gsl_vector_get(state->x,2));
		fprintf(stderr,"size= %.3f  ",state->size);
		fprintf(stderr,"fval/n= %.3f  ",state->fval/n);
		fprintf(stderr,"\n");

	}while( status == GSL_CONTINUE && iter < 100);
	
	// save results to variables
	double A=gsl_vector_get(state->x,0);
	double T=gsl_vector_get(state->x,1);
	double B=gsl_vector_get(state->x,2);

	printf("# t, A*exp(-t/T)+B\n");
	double dt=(t[n-1]-t[0])/50;
	for(double ti=t[0]; ti<t[n-1]+dt; ti+=dt) printf("%g %g\n",ti,A*exp(-ti/T)+B);

	// free allocated memory
	gsl_multimin_fminimizer_free(state);
	gsl_vector_free(start);
	gsl_vector_free(step);
	return 0;
}
