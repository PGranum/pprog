#include<stdio.h>
#include<math.h>
double integral_function(double);
double integral_function_3(double);
double integral_function_2(double);

int main(){

	fprintf(stderr,"The integral of ln(x)*x^(-1/2) from 0 to 1 is = %g\n",integral_function(0.0));

	double a=0.1, b=2.5, dx=0.013; //plot in this interval
	for(double alpha=a;alpha<b;alpha+=dx){
		double result = integral_function_3(alpha)/integral_function_2(alpha);
		printf("%g %g\n",alpha,result);
	}
		
		

return 0;
}
