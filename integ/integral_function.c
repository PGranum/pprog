#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_sf_log.h>

double integrand(double x, void* params){
	return gsl_sf_log(x)/sqrt(x);
}

double integral_function(double z){
	//if(z<0) return M_PI/sin(M_PI*z)/integral_function(1-z);

	gsl_function f;
	f.function = integrand;
	f.params = (void*)&z;

	int limit = 1000;
	double a=0,b=1,acc=1e-6,eps=1e-6,result,err;
	gsl_integration_workspace * workspace =
		gsl_integration_workspace_alloc(limit);
	int status=gsl_integration_qag(&f,a,b,acc,eps,limit,3,workspace,&result,&err);
	gsl_integration_workspace_free(workspace);
	if(status!=GSL_SUCCESS) return NAN;
	else return result;
}

