#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include "equal.h"

int main(){
	int i = 1;
	// find maximum integer using different methods
	printf("INT_MAX = %i\n",INT_MAX);

	while(i<i+1){i++;}
	printf("maximum integer is %i\n",i);

	int j = 0;
	for(int i=1; i < i+1;i++){j = i;}
	printf("maximum integer is %i\n",j+1);

	i = 0;
	do {i++;} while(i<i+1);
	printf("maximum integer is %i\n",i); 
	
	// find minimum integer using different methods
	i = 1;
	printf("INT_MIN = %i\n",INT_MIN);

	while(i-1<i){i--;}
	printf("minimum integer is %i\n",i);

	j = 0;
	for(int i=1; i-1 < i;i--){j = i;}
	printf("minimum integer is %i\n",j-1);

	i = 0;
	do {i--;} while(i-1<i);
	printf("minimum integer is %i\n",i); 



	// calculate epsilon
	printf("epsilons defined in float.h: \n   float %g\n   double %g\n   long double %Lg\n",FLT_EPSILON, DBL_EPSILON, LDBL_EPSILON);
	printf("Epsilons caldulated using while, for, and do-while loops:\n");

	float x = 1;
	while (1+x!=1){x = x/2;}
	x = x*2;
	printf("float epsilon %g\n",x);
	
	for (x = 1; 1+x != 1; x/=2);
	x = x*2;
	printf("float epsilon %g\n",x);
	
	x = 1;
	do {x/=2;} while (1+x != 1);
	x = x*2;
	printf("float epsilon %g\n",x);

	double y = 0.5;
	while (1+y!=1){y = y/2;}
    	y = y*2;
	printf("double epsilon %g\n",y);

	for (y = 1; 1+y != 1; y/=2);
	y = y*2;
	printf("double epsilon %g\n",y);
	
	y = 1;
	do {y/=2;} while (1+y != 1);
	y = y*2;
	printf("double epsilon %g\n",y);

	long double z = 0.5;
	while (1+z!=1){z = z/2;}
   	z = z*2;
	printf("long double epsilon %Lg\n",z);

	for (z = 1; 1+z != 1; z/=2);
	z = z*2;
	printf("long double epsilon %Lg\n",z);
	
	z = 1;
	do {z/=2;} while (1+z != 1);
	z = z*2;
	printf("long double epsilon %Lg\n",z);

	// question 2
	float max = INT_MAX/2;
	float sum_up_float = 0;
	for(int n = 1; n <= max; n++){sum_up_float += 1.0f/n;}
	printf("sum up float = %g\n",sum_up_float);

	float sum_down_float = 0;
	for(int m = 0; m < max; m++){sum_down_float += 1.0f/(max-m);}
	printf("sum down float = %g\n",sum_down_float);
	printf("INT_MAX is an odd number. For each term there is a small round off error, which accumulates to a significant difference\n");

	double maxd = INT_MAX/2;
	double sum_up_double = 0;
	for(int n = 1; n <= maxd; n++){sum_up_double += 1.0f/n;}
	printf("sum up double= %g\n",sum_up_double);

	double sum_down_double = 0;
	for(int m = 0; m < maxd; m++){sum_down_double += 1.0f/(maxd-m);}
	printf("sum down double= %g\n",sum_down_double);
	printf("Round off error is much smaller for double type\n");


	double a = 10;
	double b = 10.1;
	double tau = 0.1;
	double eps = 0.01;
	int is_equal = equal(a,b,tau,eps);
	printf("Return 1 if %g and %g are equal within the given tolerances: %i\n",a,b,is_equal);

	return 0;
}


